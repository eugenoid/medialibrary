//
//  ViewController.swift
//  MyMediaLibrary
//
//  Created by Eugene Migun on 15/02/2017.
//  Copyright © 2017 eugenoid. All rights reserved.
//

import UIKit

import MediaPlayer


class ViewController: UIViewController, MPMediaPickerControllerDelegate
{

	override func viewDidLoad()
	{
		super.viewDidLoad()
		
		/*MPMediaLibrary.requestAuthorization
		{ (status) in
			if(status == MPMediaLibraryAuthorizationStatus.authorized)
			{
				self.querySongs()
			}
			else
			{
				self.showError()
			}
		}
		*/
		
		
		let picker = MPMediaPickerController(mediaTypes: .music)
		
		picker.delegate = self
		
		self.present(picker, animated: true, completion: {})
		
		
	}
	
	func mediaPicker(_ mediaPicker: MPMediaPickerController, didPickMediaItems mediaItemCollection: MPMediaItemCollection)
	{
		print("didPick: \(mediaItemCollection)")
	}
	
	func mediaPickerDidCancel(_ mediaPicker: MPMediaPickerController) {
		print("didCancel")
	}
	
	
	func querySongs()
	{
		let query = MPMediaQuery.songs()

		//print("items: \(query.items)")
		
		
		for item in query.items!
		{
			print("item.title: \(item.title)")
		}
		
		
	}
	
	func showError()
	{
		var error: String
		
		switch MPMediaLibrary.authorizationStatus()
		{
		case .restricted:
			error = "access restricted"
		case .denied:
			error = "denied by user"
			
		default:
			error = "unknown error"
		}
		
		print("error: \(error)")
	}
	
	
	
	
}

